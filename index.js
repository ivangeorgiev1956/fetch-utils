export async function getJson(url) {
    const configuration = {
        method: "GET",
        headers: { "Accept": "application/json" }
    };
    const response = await fetch(url, configuration);
    const json = await response.json();

    return json;
}